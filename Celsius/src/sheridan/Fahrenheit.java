package sheridan;

public class Fahrenheit {

	public static int fromFahrenheit(int fahrenheit) {
		if (fahrenheit < 0) {
			throw new IllegalArgumentException("Negative Not Allowed");
		}
		return Math.round(fahrenheit - 32) * 5 / 9;
	}
}
