package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CelciusTest {

	@Test
	public void testfromFahrenheitRegular() {
		double celsius = Fahrenheit.fromFahrenheit(32);
		assertTrue("Fahrenheit did not match result", celsius == 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testfromFahrenheitExceptional() {
		double celsius = Fahrenheit.fromFahrenheit(-32);
		assertTrue("Fahrenheit did not match result", celsius == 21);
	}

	@Test
	public void testfromFahrenheitBoundaryIn() {
		double celsius = Fahrenheit.fromFahrenheit(60);
		assertTrue("Fahrenheit did not match result", celsius == 21);
	}

	@Test
	public void testfromFahrenheitBoundaryOut() {
		double c1 = Fahrenheit.fromFahrenheit(60);
		double c2 = Fahrenheit.fromFahrenheit(61);
		assertFalse("Fahrenheit did not match", c1 != c2);
	}

}
